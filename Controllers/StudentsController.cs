﻿using AngularJs_StudentCRUD.Interface;
using AngularJs_StudentCRUD.Models;
using AngularJs_StudentCRUD.repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AngularJs_StudentCRUD.Controllers
{

 

    public class StudentsController : ApiController
    {

        static readonly IStudentRepository studentResp = new StudentRepository();

        [Route("api/Students/GetAllStudents")]
        [ResponseType(typeof(Student))]
        [HttpGet]
        public IEnumerable<Student> GetAllStudents()
        {
          return  studentResp.GetAll();
        }

      
        [Route("api/Students/GetStudentByID/{Id:int}")]
        [ResponseType(typeof(Student))]
        [HttpGet]
        public Student GetStudentByID(int id)
        {
            return studentResp.Get(id);
        }




        [Route("api/Students/AddStudent")]
        [ResponseType(typeof(Student))]
        [HttpPost]

        public  Student AddStudent(Student item)
        {
            return studentResp.Add(item);
        }


        [Route("api/Students/UpdateStudent")]
        [ResponseType(typeof(Student))]
        [HttpPut]
        public bool UpdateStudent(Student item)
        {
            return studentResp.Update(item);
        }


        [Route("api/Students/DeleteStudent/{Id:int}")]
        [ResponseType(typeof(Student))]
        [HttpDelete]
        public bool DeleteStudent(int id)
        {
            return studentResp.Delete(id);
        }




    }
}
