﻿using AngularJs_StudentCRUD.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Web;

namespace AngularJs_StudentCRUD.Interface
{
    public  interface IStudentRepository
    {
        IEnumerable<Student> GetAll();


        Student Get(int id);

        Student Add(Student item);

        bool Update(Student item);

        bool Delete(int id);


    }
}