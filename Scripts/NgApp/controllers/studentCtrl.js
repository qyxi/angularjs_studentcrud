﻿
app.controller('studentCtrl', ['$scope', 'studentService',
    function ($scope, studentService) {

        // Base Url 
        var baseUrl = '/api/Students/';
        $scope.btnText = "Save";
        $scope.studentID = 0;
        $scope.SaveUpdate = function () {
            var student = {             

                Name: $scope.Name,
                Department: $scope.Department,
                Phone: $scope.Phone,
                Year: $scope.Year,
                Id: $scope.Id
            }
            if ($scope.btnText == "Save") {
                var apiRoute = baseUrl + 'AddStudent/';
                var saveStudent = studentService.post(apiRoute, student);
                saveStudent.then(function (response) {
                    if (response.data != "") {
                        alert("Data Save Successfully");
                        $scope.GetStudents();
                        $scope.Clear();

                    } else {
                        alert("Some error");
                    }

                }, function (error) {
                    console.log("Error: " + error);
                });
            }
            else {
                var apiRoute = baseUrl + 'UpdateStudent/';
                var UpdateStudent = studentService.put(apiRoute, student);
                UpdateStudent.then(function (response) {
                    if (response.data != "") {
                        alert("Data Update Successfully");
                        $scope.GetStudents();
                        $scope.Clear();

                    } else {
                        alert("Some error");
                    }

                }, function (error) {
                    console.log("Error: " + error);
                });
            }
        }


        $scope.GetStudents = function () {
            var apiRoute = baseUrl + 'GetAllStudents/';
            var student = studentService.getAll(apiRoute);
            student.then(function (response) {
               // debugger
                $scope.studnets = response.data;

            },
                function (error) {
                    console.log("Error: " + error);
                });


        }
        $scope.GetStudents();

        $scope.GetStudentByID = function (dataModel) {
           // debugger
            var apiRoute = baseUrl + 'GetStudentByID';
            var student = studentService.getbyID(apiRoute, dataModel.Id);
            student.then(function (response) {

             

                $scope.Id = response.data.Id;
                $scope.Name = response.data.Name;
                $scope.Department = response.data.Department;
                $scope.Phone = response.data.Phone;
                $scope.Year = response.data.Year;
                $scope.btnText = "Update";
            },
                function (error) {
                    console.log("Error: " + error);
                });
        }

        $scope.DeleteStudent = function (dataModel) {
            debugger
            var apiRoute = baseUrl + 'DeleteStudent/' + dataModel.Id;
            var deleteStudent = studentService.delete(apiRoute);
            deleteStudent.then(function (response) {
                if (response.data != "") {
                    alert("Data Delete Successfully");
                    $scope.GetStudents();
                    $scope.Clear();

                } else {
                    alert("Some error");
                }

            }, function (error) {
                console.log("Error: " + error);
            });
        }

        $scope.Clear = function () {
            $scope.Id = 0;
            $scope.Name = "";
            $scope.Department = "";
            $scope.Phone = "";
            $scope.Year = "";
            $scope.btnText = "Save";
        }

    }]);