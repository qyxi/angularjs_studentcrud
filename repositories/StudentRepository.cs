﻿using AngularJs_StudentCRUD.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AngularJs_StudentCRUD.Models;
using System.Data.Entity.Core.Metadata.Edm;
using System.Runtime.InteropServices;
using System.Net.Http;
using System.Data.Entity;
using System.Net;

namespace AngularJs_StudentCRUD.repositories
{
    
    
    public class StudentRepository : IStudentRepository
    {
        StudentsEntities students = new StudentsEntities();

        public IEnumerable<Student> GetAll()
        {
            //return students.Students;

            IEnumerable<Student> AllStudents = null;
            try
            {
                AllStudents = students.Students;

            }
            catch (Exception e)
            {
                AllStudents = null;
            }

            return AllStudents;

        }


        public Student Get(int id)
        {

            //return students.Students.Where(a => a.Id == id).FirstOrDefault();

            Student student = null;
            try
            {
                student =  students.Students.Where(a => a.Id == id).SingleOrDefault();

            }
            catch (Exception e)
            {
                student = null;
            }

            return student;


        }

        public Student Add(Student item)
        {

           //if (item == null)
           //     throw new ArgumentNullException("item null");
           //students.Students.Add(item);
           //students.SaveChanges();
           //return item;
                                  
            try
            {
                
                students.Students.Add(item);
                students.SaveChanges();               
            }
            catch (Exception e)
            {
                return null;
            }

            return item;



        }

        public bool Update(Student item)
        {
            if (item == null)
                throw new ArgumentException("item null"); 
            Student s = new Student(); 


            s = students.Students.Where(a => a.Id == item.Id).FirstOrDefault();
            s.Name = item.Name;
            s.Phone = item.Phone;
            s.Department = item.Department;
            s.Year = item.Year;

            students.SaveChanges();

            return true;
        }

    public bool Update2(Student item)
        {
            
            try
            {
                students.Students.Attach(item);
                students.Entry(item).State = EntityState.Modified;
                students.SaveChanges();
                
            }
            catch (Exception e)
            {

                return false;
            }

            return true; // Request.CreateResponse(HttpStatusCode.OK, result);
        }

        public bool Delete(int id)
        {           
        //    students.Students.Remove(students.Students.Where(a => a.Id == id).FirstOrDefault());

            var student = students.Students.Where(x => x.Id == id).FirstOrDefault();
            students.Students.Attach(student);
            students.Students.Remove(student);
            students.SaveChanges();
       

            return true;
        }

    }
}